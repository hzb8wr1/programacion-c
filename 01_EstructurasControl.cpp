#include <stdio.h>
#include <stdlib.h>


int main () {

//Si en x pones un 3 (numero positivo) imprime por pantalla un 5
//si en x pones un -3 (numero negativo) imprime por pantalla un -5 
	
	
int xmin = 5;
int x = -3;	

if(abs(x)<xmin) x=(x>0)?xmin:-xmin;

printf("%i\n",x);

return EXIT_SUCCESS;
}
