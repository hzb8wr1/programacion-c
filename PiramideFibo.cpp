#include <stdio.h>
#include <stdlib.h>

#define N 15

int main () {

  int array [N][N];

  for(int i = 0;i < N;i++){         //Bucle rellena de 0s el array
      for(int z = 0; z < N;z++)
          array[i][z] = 0;

      array [i][0] = 1;              //Coloco un uno en la primera columna de cada fila
  }

  for(int i = 1;i < N;i++)         //Algoritmo para calculo de la pirámide de Fibonacci
      for(int z = 1;z < N;z++)     //Empezamos el bucle en la segunda columna, porque el primer valor no cambiará
          array[i][z] = array [i-1][z] + array[i-1][z-1];

  for(int i = 0;i < N;i++){
      for(int z = 0; z < N;z++)
          printf("%i\t",array[i][z]);

      printf("\n");
  }

    return EXIT_SUCCESS;
}
