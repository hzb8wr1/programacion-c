#include <stdio.h>
#include <stdlib.h>

    //Ordenación por insercción Menor a mayor

    int main () {

    int pausa;
    int Guardador;
    int numero_mayor = 0;
    int z = 0;
    int numeros [] = {12,5,4,7,14,25,50,6};

    for(int i = 0;i < 8;i++){
        numero_mayor = numeros[i];                    //Busca el numero mayor, cuando lo encuentra lo pasa a la posición 1.
        for(z = i+1;z < 8;z++)                       //En el siguiente ciclo la posición 1 sera ignorada
           if(numero_mayor > numeros[z]){               //Si cambias el simbolo > por < la ordenacion pasa a ser de mayor a menor.
                   Guardador    = z;
                   numero_mayor = numeros[z];
            }
        numeros [Guardador]  = numeros [i];
        numeros [i]  = numero_mayor;
    }

    for(int k = 0;k < 8;k++)
        printf("%i\n",numeros[k]);

  return EXIT_SUCCESS;

  }
