#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Borrador espacios en blanco
char *borrador_espacios(char*);
char *borrador_izq(char*);
char *borrador_drc(char*);

int main ()  {

  char cadena [] = "    Que tragedia encontrar tanto null   ";
  borrador_espacios(cadena);
  printf("[%s]\n",cadena);

 return EXIT_SUCCESS;

}

char *borrador_espacios(char *cadena){

  return borrador_izq(borrador_drc(cadena));
}

char *borrador_izq(char *cadena){

    char *ptr = cadena;
    int espacios = 0;

                                              //se cuentan los espacios, que influiran en el numero de iteraciones
    for(;ptr != 0;ptr++){
        if(*ptr == 32)
            espacios++;
        else
            break;
    }

    for(int i = 0;i < strlen(cadena)+1-espacios;i++)   //Se intercambian la posicion con el que tengan delante
        cadena [i] = cadena [i+espacios];              //tantas veces como numero de espacios por delante en la izquierda

 return cadena;

}

char *borrador_drc(char *cadena){

 char *ptr = cadena;
 ptr += strlen(cadena)-1;

 for(int i = 0;i < strlen(cadena);i++,ptr--){           //Aquí se encuentran los espacios a traves de un if dentro del for
     if(*ptr == 32)                                     //se sustituye donde se encuentren por un /0 acortando la cadena.
        *ptr = 0;
     else
         break;
  }

 return cadena;
}

