#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {

  char texto [] = "Castrobarto";
  char *ptr = texto;
  char *bptr = texto;
  char guardador;
  int  numero_iteraciones = strlen(texto)/2;   //Dividiendo entro dos el tamaño de la cadena obtenemos el numero de iteraciones
                                               // necesarias en el bucle, las letras se cambian de dos en dos, por eso entre dos.
  bptr += strlen(texto)-1;

  for(int i = 0;i < numero_iteraciones;i++,ptr++,bptr--){
      guardador  = *bptr;
       *bptr      = *ptr;
       *ptr       = guardador;
  }
 
  printf("\n%s\n",texto);

 return EXIT_SUCCESS;

}


